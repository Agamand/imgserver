var path = require('path'),
    url = Core.Route.url,
    res = Core.Route.resource,
    action = Core.Route.action;
Core.configure({
    namespace: {
        'ImgServer': path.normalize(__dirname + '/../')
    },
    settings: {
        cache: {
            cacheDir: path.normalize(__dirname + '/../cache/'),
            duration: 1000 * 60 * 10,
            cacheControl: 1000 * 60 * 10
        },
        dataDir: path.normalize(__dirname + '/../data/')
    },
    port: 9001,
    pathPrefix: 'ImgServer',
    routing: {
        '//crop//%([0-9]+|-)//%([0-9]+|-)//%(.*)': action('ImgServer.Resize', {
            path: '$2',
            x: '$0',
            y: '$1',
            crop: 1
        }),
        '//resize//%([0-9]+|-)//%([0-9]+|-)//%(.*)': action('ImgServer.Resize', {
            x: '$0',
            y: '$1',
            path: '$2'
        }),
        '//%(.*)': action('ImgServer.Resize', {
            path: '$0'
        })

    }
});