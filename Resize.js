var gm = require('gm'),
    fs = require('fs'),
    path = require('path'),
    async = Core.Async,
    imgSetting = Core.getConfig('settings'),
    cache = imgSetting.cache,
    execFile = require('child_process').execFile,
    crypto = require('crypto'),
    request = Core.Request;

Core.define('ImgServer.Resize', {
    extendOf: 'AgmdJS.template.Action',
    proceed: function(req, response) {
        var me = this,
            incoming,
            cfg = {};
        incoming = req._GET;
        console.log(incoming);
        //Logger.debug('resize income', incoming);
        if (!incoming.path) {
            Logger.error('incoming err', incoming);
            me.sendEmptyImg();
            return;
        }
        var opt = {
            upscale: true,
            crop: incoming.crop == '1' || incoming.crop == 'true'
        };
        if (incoming.upscale && (incoming.upscale == 'true' || incoming.upscale == 'false'))
            opt.upscale = incoming.upscale == 'true';

        var extern = /^http?/.exec(incoming.path);
        if (extern && 0 == extern.index) {
            Logger.debug('EXTERNAL FILE TO HANDLE');

        }


        cfg.x = +incoming.x;
        cfg.x = isNaN(cfg.x) || !cfg.x ? null : cfg.x;
        cfg.y = +incoming.y;
        cfg.y = isNaN(cfg.y) || !cfg.y ? null : cfg.y;
        cfg.path = incoming.path;

        cfg.origin = extern ? cfg.path : path.normalize(imgSetting.dataDir + '/' + cfg.path);

        var shasum = crypto.createHash('md5');
        var fileName = shasum.update(incoming.path).digest('hex');
        cfg.out = path.normalize(cache.cacheDir + '/' + (opt.crop ? 'crop' : 'resize') + '/' + cfg.x + "-" + cfg.y + "/" + fileName);
        Logger.debug('out path', cfg.out);
        Core.Fs.makePathSync(cfg.out);

        var today = new Date();
        var fileStat,
            cacheExist,
            format;
        async.series([function(callback) {
            //First check if file exist
            fs.exists(cfg.out, function(exist) {
                cacheExist = exist;
                callback();
            });

        }, function(callback) {
            if (cacheExist) {
                fs.stat(cfg.out, function(err, stat) {
                    fileStat = stat;
                    Logger.debug('file stats', fileStat, (today - fileStat.ctime) < cache.duration);
                    if (cacheExist && fileStat && ((today - fileStat.ctime) < cache.duration || cache.duration < 0)) {
                        Logger.info('already in cache send immediatly (time remaining -> ', cache.duration - (today - stat.ctime) / 1000, 'seconds)');
                        callback('ready');
                        return;
                    } else {
                        callback();
                    }
                });
            } else callback();
        }, function(callback) {
            me.generateCache(cfg, extern, opt, function(err, out, _format) {
                fileStat = {
                    mtime: new Date()
                }
                format = _format
                if (err) {
                    callback('fail');
                } else {
                    callback('ready');
                }
            })
        }], function(_break) {
            if ('ready' == _break) {
                //check client cache
                var client_last_modified = req.headers['if-modified-since'] && new Date(req.headers['if-modified-since']) || null;
                me.debug('already in cache check client date');
                me.debug('client_last_modified:', client_last_modified, 'fileStat.mtime:', fileStat.mtime, 'diff:', Math.ceil((client_last_modified - fileStat.mtime) / 1000))
                if (client_last_modified && Math.ceil((client_last_modified - fileStat.mtime) / 1000) >= 0) {
                    me.sendNotModified({
                        format: format,
                        stat: fileStat
                    });
                } else {
                    me.send(cfg.out, {
                        format: format,
                        stat: fileStat
                    })
                }

            } else if ('fail' == _break) ;
        })

    },
    generateCache: function(cfg, extern, opt, callback) {

        var me = this;
        Logger.debug('generateCache', cfg, opt);
        me.resize(cfg.origin, cfg.x, cfg.y, extern, opt, function(img, format) {
            // Save;
            img.write(cfg.out, function(err) {
                if (err) {
                    Logger.error("err while writing img", err);
                    callback(err);
                    return;
                }
                // If jpeg, optimize size.
                if (format == 'jpeg' && imgSetting.jpegoptim) {
                    var args = [
                        '-o',
                        '--strip-all',
                        '--strip-iptc',
                        '--strip-icc',
                        '--all-progressive',
                        //  '-d'+cfg.out,
                        cfg.out
                    ];

                    execFile('jpegoptim', args, function(err) {
                        if (err) {
                            throw err;
                        }
                        if (!err) {
                            callback(null, cfg.out, format);
                        } else
                            callback(err);
                    });
                } else if (format == 'png' && imgSetting.optipng) {
                    var args = [
                        '-o2',
                        cfg.out
                    ];
                    // Logger.info('use optipng');
                    execFile('optipng', args, function(err) {
                        if (err) {
                            throw err;
                        }
                        // Logger.info('out of optipng', err);
                        if (!err) {
                            callback(null, cfg.out, format);
                        } else
                            callback(err);
                    });
                } else callback(null, cfg.out, format);
            });
        });
    },
    getStream: function(path, extern, callback) {
        var me = this;

        // Logger.info('Extern ?', extern);
        if (!extern) {
            fs.exists(path, function(exist) {
                if (!exist) {
                    Logger.error(path, 'not found');
                    me.sendEmptyImg();
                } else callback(null, fs.createReadStream(path));
            });
        } else {
            request.get(path, function(err, req, res) {
                console.log('res statusCode: ', res);
                if (err)
                    console.log('error', err)
                callback(err, res);
            });
        }
    },
    resize: function(path, x, y, extern, opt, cb) {
        var me = this;
        if (!opt.crop) {
            me.getStream(path, extern, function(error, stream) {
                var img = gm(stream);
                img.identify({
                    bufferStream: true
                }, function(err, info) {
                    img.size({
                        bufferStream: true
                    }, function(err, v) {
                        if (!x && !y)
                            cb(img, info.format.toLowerCase());
                        else {
                            if (x > v.width && !opt.upscale)
                                x = v.width;
                            if (y > v.height && !opt.upscale)
                                y = v.height;
                            cb(img.resize(x, y), info.format.toLowerCase());
                        }
                    });
                });
            });
        } else {
            me.getStream(path, extern, function(error, stream) {
                var img = gm(stream);
                img.identify({
                    bufferStream: true
                }, function(err, info) {
                    img.size({
                        bufferStream: true
                    }, function(err, v) {
                        if (!v || !v.width || !v.height) {
                            me.sendEmptyImg();
                            return;
                        }
                        x = x || v.width;
                        y = y || v.height;

                        // Logger.debug(x, y, v, opt);
                        if (x > v.width && !opt.upscale)
                            x = v.width;
                        if (y > v.height && !opt.upscale)
                            y = v.height;

                        var a = y / v.height,
                            b = x / v.width;


                        var _x = x,
                            _y = y,
                            offset = {
                                x: 0,
                                y: 0
                            };
                        // Logger.debug(x, y, v);
                        if (a < b)
                            offset.y = ((_y = b * v.height) - y) / 2;
                        else
                            offset.x = ((_x = a * v.width) - x) / 2;


                        var vpadding = (v.height - y) / 2,
                            coef = ((opt.offset - 50) / 50)

                        offset.y += vpadding * coef;

                        // Logger.debug(_x, _y, offset);
                        cb(img.resize(_x, _y).crop(x, y, offset.x, offset.y), info.format.toLowerCase());
                    });
                });
            });
        }
    },
    makePath: function(path, callback) {
        path = path.split('/');
        var p = "";
        for (var i = 0, len = path.length - 1; i < len; i++) {
            if (!path[i].length)
                continue;
            p += '/' + path[i];
            if (!fs.existsSync(p))
                fs.mkdirSync(p);
        }
    },
    formHeader: function(imgInfo) {
        var headers = {
            'Content-Type': ['image/', imgInfo.format].join('')
        };
        if (cache.cacheControl > 0) {

            headers['Cache-Control'] = 'public, must-revalidate, proxy-revalidate';
            headers['Expires'] = new Date(+imgInfo.stat.mtime + cache.cacheControl);
            headers['Last-Modified'] = imgInfo.stat.mtime;
        }
        return headers;
    },
    send: function(path, imgInfo) {
        var me = this,
            //
            fileName = path.split();
        // console.log(img);
        if (imgInfo.format) {
            var img = fs.readFileSync(path);
            me.response.writeHead(200, me.formHeader(imgInfo));
            me.response.end(img, 'binary');
        } else {
            gm(path).identify(function(err, info) {
                if (err)
                    me.error(err);
                imgInfo.format = info.format.toLowerCase();
                var img = fs.readFileSync(path);
                me.response.writeHead(200, me.formHeader(imgInfo));
                me.response.end(img, 'binary');
            })
        }
    },
    sendNotModified: function(imgInfo) {
        var me = this;
        me.response.writeHead(304, {
            'Cache-Control': 'public, must-revalidate, proxy-revalidate',
            'Expires': new Date(+imgInfo.stat.mtime + cache.cacheControl),
            'Last-Modified': imgInfo.stat.mtime
        });
        me.response.end();
    },
    sendEmptyImg: function() {
        var me = this;
        me.fireEvent('notfound');
    }

});